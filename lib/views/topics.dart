import 'package:finki_forum_flutter/services/route_generator.dart';
import 'package:flutter/material.dart';
import 'package:finki_forum_flutter/views/shared/my_scaffold.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'dart:convert';
import 'package:finki_forum_flutter/assets/constants.dart' as Constants;
import 'package:finki_forum_flutter/models/auth.dart';

class Topics extends StatefulWidget {
  final String sid;

  const Topics({
    Key? key,
    required this.sid,
  }) : super(key: key);

  @override
  State<Topics> createState() => _TopicsState();
}

class _TopicsState extends State<Topics> {
  List _topics = [];

  @override
  void initState() {
    super.initState();

    fetchTopics().then((value) {
      print('Topics fetched');
    }).catchError((error) {
      print('Topics fetch error');
    });
  }

  Future<void> fetchTopics() async {
    var request = http.MultipartRequest(
      'POST',
      Uri.parse('${Constants.SERVER_ADDRESS}/topics/getTopicsListMobile'),
    );
    request.fields['subforum_id'] = widget.sid;
    request.fields['from'] = '0';
    request.fields['limit'] = '20';
    var response = await request.send();
    var responsed = await http.Response.fromStream(response);
    final responseData = jsonDecode(responsed.body);
    setState(() {
      _topics = responseData;
    });
  }

  Widget _itemBuilder(BuildContext context, int index) {
    return ListTile(
        leading: Icon(Icons.description),
        title: Text(_topics[index]['title']),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                  child: Text(
                    'Прегледи: ${_topics[index]['views_num']}',
                    style: TextStyle(fontSize: 12),
                  ),
                ),
                Text(
                  'Мислења: ${_topics[index]['posts_num']}',
                  style: TextStyle(fontSize: 12),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Text(
                  'Креирана од: ${_topics[index]['creation_data']['username']} (${_topics[index]['creation_data']['date']})',
                  style: TextStyle(fontSize: 12),
                ),
              ],
            ),
            if (_topics[index].containsKey('last_post'))
              Text(
                'Последно мислење: ${_topics[index]['last_post']['username']} (${_topics[index]['last_post']['date']})',
                style: TextStyle(fontSize: 12),
              ),
          ],
        ),
        onTap: () {
          Navigator.pushNamed(
            context,
            '/posts',
            arguments: PostsScreenArguments(
                sid: widget.sid,
                tid: _topics[index]['_id']['\$id'],
                topicTitle: _topics[index]['title']),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    // Scaffold is a layout for the major Material Components.
    return MyScaffold(
      title: 'Topics View',
      // body is the majority of the screen.
      body: new ListView.builder(
        itemCount: _topics.length,
        itemBuilder: _itemBuilder,
      ),
      fab: context.watch<AuthModel>().isLoggedIn
          ? FloatingActionButton(
              onPressed: () {
                // Add your onPressed code here!
                Navigator.pushNamed(
                  context,
                  '/add-topic',
                  arguments: TopicsScreenArguments(
                    sid: widget.sid,
                  ),
                );
              },
              child: Icon(Icons.add_circle),
            )
          : null,
      fabLocation: context.watch<AuthModel>().isLoggedIn
          ? FloatingActionButtonLocation.centerFloat
          : null,
    );
  }
}
