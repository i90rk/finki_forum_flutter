import 'package:flutter/material.dart';
import 'package:finki_forum_flutter/views/shared/my_scaffold.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:finki_forum_flutter/assets/constants.dart' as Constants;
import 'package:finki_forum_flutter/models/auth.dart';
import 'package:provider/provider.dart';

class Login extends StatefulWidget {
  const Login({
    Key? key,
  }) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final email = TextEditingController(text: 'admin');
  final password = TextEditingController(text: 'admin123');

  Future<void> loginHandler() async {
    if (email.text.isEmpty || password.text.isEmpty) {
      print('Login: Validation Failed');
      return null;
    }

    var request = http.MultipartRequest(
      'POST',
      Uri.parse('${Constants.SERVER_ADDRESS}/global_actions/verifyUserMobile'),
    );
    request.fields['username'] = email.text;
    request.fields['password'] = password.text;
    request.fields['api_key'] = Constants.API_KEY;
    var response = await request.send();
    var responsed = await http.Response.fromStream(response);
    final responseData = jsonDecode(responsed.body);
    try {
      var authContext = context.read<AuthModel>();
      authContext.setAuthData(
          responseData['token'], responseData['userdata'], true);
      Navigator.pushNamed(
        context,
        '/',
      );
    } catch (err) {
      print('Login error');
      print(err);
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      title: 'Логирај се',
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              children: <Widget>[
                TextField(
                  controller: email,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Kорисничко име',
                    hintText: 'Внеси корисничко име',
                  ),
                ),
                SizedBox(height: 10),
                TextField(
                  controller: password,
                  obscureText: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Лозинка',
                    hintText: 'Внеси безбедна лозинка',
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  height: 50,
                  width: 250,
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: TextButton(
                    child: Text(
                      'Логирај се',
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                    onPressed: () {
                      loginHandler();
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
