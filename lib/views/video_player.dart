import 'dart:async';

import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class VideoPlayerScreen extends StatefulWidget {
  final String url;
  const VideoPlayerScreen({
    Key? key,
    required this.url,
  }) : super(key: key);

  @override
  _VideoPlayerScreenState createState() => _VideoPlayerScreenState();
}

class _VideoPlayerScreenState extends State<VideoPlayerScreen> {
  bool _onTouch = false;
  Timer? _timer;
  late VideoPlayerController _controller;

  @override
  void initState() {
    // Create and store the VideoPlayerController. The VideoPlayerController
    // offers several different constructors to play videos from assets, files,
    // or the internet.
    _controller = VideoPlayerController.network(
      widget.url,
    );

    _controller.initialize().then((_) {
      setState(() {});
    });

    super.initState();
  }

  @override
  void dispose() {
    // Ensure disposing of the VideoPlayerController to free up resources.
    _controller.dispose();
    _timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return _controller.value.isInitialized
        ? GestureDetector(
            onTap: () {
              _timer?.cancel();
              setState(() {
                _onTouch = true;
              });
              _timer = Timer.periodic(Duration(milliseconds: 2000), (_) {
                setState(() {
                  _onTouch = false;
                });
              });
            },
            child: Stack(children: <Widget>[
              AspectRatio(
                aspectRatio: _controller.value.aspectRatio,
                // Use the VideoPlayer widget to display the video.
                child: VideoPlayer(_controller),
              ),
              AspectRatio(
                aspectRatio: _controller.value.aspectRatio,
                child: Visibility(
                  visible: _onTouch,
                  child: Center(
                    child: ElevatedButton(
                      onPressed: () {
                        _timer?.cancel();

                        // pause while video is playing, play while video is pausing
                        setState(() {
                          _controller.value.isPlaying
                              ? _controller.pause()
                              : _controller.play();
                        });

                        // Auto dismiss overlay after 2 second
                        _timer =
                            Timer.periodic(Duration(milliseconds: 2000), (_) {
                          setState(() {
                            _onTouch = false;
                          });
                        });
                      },
                      child: Icon(
                        _controller.value.isPlaying
                            ? Icons.pause
                            : Icons.play_arrow,
                        color: Colors.white,
                      ),
                      style: ElevatedButton.styleFrom(
                        shape: CircleBorder(),
                        padding: EdgeInsets.all(20),
                        primary: Colors.grey.withOpacity(0.9),
                        onPrimary: Colors.red,
                      ),
                    ),
                  ),
                ),
              ),
            ]))
        : Container();
  }
}
