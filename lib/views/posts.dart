import 'package:finki_forum_flutter/services/route_generator.dart';
import 'package:finki_forum_flutter/views/video_player.dart';
import 'package:flutter/material.dart';
import 'package:finki_forum_flutter/views/shared/my_scaffold.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'dart:convert';
import 'package:finki_forum_flutter/assets/constants.dart' as Constants;
import 'package:finki_forum_flutter/models/auth.dart';

class Posts extends StatefulWidget {
  final String sid;
  final String tid;
  final String topicTitle;

  const Posts({
    Key? key,
    required this.sid,
    required this.tid,
    required this.topicTitle,
  }) : super(key: key);

  @override
  State<Posts> createState() => _PostsState();
}

class _PostsState extends State<Posts> {
  List _posts = [];

  @override
  void initState() {
    super.initState();

    fetchPosts().then((value) {
      print('Posts fetched');
    }).catchError((error) {
      print('Posts fetch error');
    });
  }

  Future<void> fetchPosts() async {
    var request = http.MultipartRequest(
      'POST',
      Uri.parse('${Constants.SERVER_ADDRESS}/posts/getPostsListMobile'),
    );
    request.fields['topic_id'] = widget.tid;
    request.fields['from'] = '0';
    request.fields['limit'] = '20';
    var response = await request.send();
    var responsed = await http.Response.fromStream(response);
    final responseData = jsonDecode(responsed.body);
    setState(() {
      _posts = responseData;
    });
  }

  String stripHTML(String post) {
    RegExp exp = RegExp(
      r'<[^>]*>',
      multiLine: true,
      caseSensitive: true,
    );
    post = post.replaceAll(exp, '');

    exp = RegExp(
      r'\[quote\].*\[\/quote\]',
      multiLine: true,
      caseSensitive: true,
    );
    post = post.replaceAll(exp, '');
    return post.trim();
  }

  Widget parseQuote(String post) {
    String? quoteUsername = '';
    String? quotePost = '';

    RegExp exp = RegExp(
      r'\[quoteUsername\](.*?)\[\/quoteUsername\]',
      multiLine: true,
      caseSensitive: true,
    );
    quoteUsername = exp.allMatches(post).elementAt(0).group(1);

    exp = RegExp(
      r'\[quotePost\](.*?)\[\/quotePost\]',
      multiLine: true,
      caseSensitive: true,
    );
    quotePost = exp.allMatches(post).elementAt(0).group(1);

    return Container(
        margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
        padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
        decoration: new BoxDecoration(
          color: Color(0xFFd9edf7),
          border: Border.all(
            color: Color(0xFFd0ecf5),
          ),
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
              child: Row(
                children: [
                  Text(
                    'испратено од: ',
                    style: TextStyle(
                      color: Color(0xFF808080),
                    ),
                  ),
                  Text(
                    '$quoteUsername',
                    style: TextStyle(
                      color: Color(0xFF808080),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              decoration: new BoxDecoration(
                border: Border(
                  left: BorderSide(
                    color: Color(0xFF808080),
                    width: 3.0,
                  ),
                ),
              ),
            ),
            SizedBox(height: 15),
            Container(
              padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
              child: Text(
                '$quotePost',
                style: TextStyle(
                  color: Color(0xFF808080),
                ),
              ),
            )
          ],
        ));
  }

  Future<void> likePost(String postID, String userID) async {
    try {
      var request = http.MultipartRequest(
        'POST',
        Uri.parse('${Constants.SERVER_ADDRESS}/posts/likePostMobile'),
      );
      var userdata = context.read<AuthModel>().userdata;
      request.fields['post_id'] = postID;
      request.fields['user_id'] = userID;
      request.fields['loggedin_user_id'] = userdata['id']['\$id'];
      request.fields['username'] = userdata['username'];
      request.fields['avatar_image'] = userdata['avatar_image'];
      request.fields['group_type'] = userdata['group_type'];
      await request.send();
      await fetchPosts();
    } catch (err) {
      print('Like post error');
      print(err);
    }
  }

  Future<void> unlikePost(String postID, String userID) async {
    try {
      var request = http.MultipartRequest(
        'POST',
        Uri.parse('${Constants.SERVER_ADDRESS}/posts/unlikePostMobile'),
      );
      var userdata = context.read<AuthModel>().userdata;
      request.fields['post_id'] = postID;
      request.fields['user_id'] = userID;
      request.fields['like_user_id'] = userdata['id']['\$id'];
      await request.send();
      await fetchPosts();
    } catch (err) {
      print('Unlike post error');
      print(err);
    }
  }

  Widget _itemBuilder(BuildContext context, int index) {
    return Card(
      child: Padding(
        padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
        child: Column(
          children: <Widget>[
            ListTile(
              leading: Icon(
                Icons.account_circle,
                size: 40,
              ),
              title: Text(
                _posts[index]['user_data']['username'],
              ),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    _posts[index]['user_data']['group_type'],
                  ),
                  Text(
                    _posts[index]['date'],
                  ),
                ],
              ),
            ),
            Divider(
              color: Colors.grey[400],
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  if (_posts[index]['post'].contains('[quote]'))
                    parseQuote(_posts[index]['post']),
                  Text(
                    stripHTML(_posts[index]['post']),
                  ),
                  SizedBox(height: 15),
                  if (_posts[index]['image_data'] != null)
                    Image.network(
                      '${Constants.SERVER_ADDRESS}/${_posts[index]['image_data']['image_path']}',
                    ),
                  if (_posts[index]['video_data'] != null)
                    VideoPlayerScreen(
                      url:
                          '${Constants.SERVER_ADDRESS}/${_posts[index]['video_data']['video_path']}',
                    ),
                  SizedBox(height: 15),
                  if (_posts[index]['likes_num'] > 0)
                    Text(
                      _posts[index]['likes_num'] == 1
                          ? '${_posts[index]['likes_num']} допаѓање'
                          : '${_posts[index]['likes_num']} допаѓања',
                      style: TextStyle(
                        color: Colors.grey[600],
                      ),
                    ),
                ],
              ),
            ),
            if (context.watch<AuthModel>().isLoggedIn) // is logged in
              Column(
                children: <Widget>[
                  Divider(
                    color: Colors.grey[400],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      _posts[index].containsKey('likes_users') &&
                              _posts[index]['likes_users'].any((el) =>
                                  el['id']['\$id'] ==
                                  context.read<AuthModel>().userdata['id']
                                      ['\$id'])
                          ? IconButton(
                              icon: const Icon(Icons.thumb_up),
                              color: Colors.blue,
                              onPressed: () {
                                unlikePost(_posts[index]['_id']['\$id'],
                                    _posts[index]['user_data']['id']['\$id']);
                              },
                            )
                          : IconButton(
                              icon: const Icon(Icons.thumb_up_off_alt),
                              color: Colors.blue,
                              onPressed: () {
                                likePost(_posts[index]['_id']['\$id'],
                                    _posts[index]['user_data']['id']['\$id']);
                              },
                            ),
                      IconButton(
                        icon: const Icon(Icons.format_quote),
                        color: Colors.blue,
                        onPressed: () {
                          Navigator.pushNamed(
                            context,
                            '/add-post',
                            arguments: PostsScreenArguments(
                                sid: widget.sid,
                                tid: widget.tid,
                                topicTitle: widget.topicTitle,
                                quoteData: {
                                  'username': _posts[index]['user_data']
                                      ['username'],
                                  'post': _posts[index]['post'],
                                }),
                          );
                        },
                      ),
                    ],
                  ),
                ],
              ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      title: 'Posts View',
      body: ListView.builder(
        itemCount: _posts.length,
        itemBuilder: _itemBuilder,
      ),
      fab: context.watch<AuthModel>().isLoggedIn
          ? FloatingActionButton(
              onPressed: () {
                // Add your onPressed code here!
                Navigator.pushNamed(
                  context,
                  '/add-post',
                  arguments: PostsScreenArguments(
                    sid: widget.sid,
                    tid: widget.tid,
                    topicTitle: widget.topicTitle,
                  ),
                );
              },
              child: Icon(Icons.add_circle),
            )
          : null,
      fabLocation: context.watch<AuthModel>().isLoggedIn
          ? FloatingActionButtonLocation.centerFloat
          : null,
    );
  }
}
