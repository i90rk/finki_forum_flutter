import 'package:flutter/material.dart';
import 'package:finki_forum_flutter/views/shared/my_scaffold.dart';
import 'package:group_list_view/group_list_view.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:finki_forum_flutter/assets/constants.dart' as Constants;
import 'package:finki_forum_flutter/services/route_generator.dart';

class Home extends StatefulWidget {
  const Home({
    Key? key,
  }) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  Map<String, List> _forums = {};

  @override
  void initState() {
    super.initState();

    fetchForums().then((value) {
      print('Forums fetched');
    }).catchError((error) {
      print('Forums fetch error');
    });
  }

  Future<void> fetchForums() async {
    final response = await http.get(
      Uri.parse('${Constants.SERVER_ADDRESS}/home/getAllForumsSubforums'),
    );
    var jsonResp = jsonDecode(response.body);
    Map<String, List> data = {};
    jsonResp['forums'].forEach((value) {
      data[value['title']] = value['subforums'];
    });
    setState(() {
      _forums = data;
    });
  }

  Widget _itemBuilder(BuildContext context, IndexPath index) {
    var subforum = _forums.values.toList()[index.section][index.index];
    return ListTile(
        leading: Icon(Icons.description),
        title: Text(subforum['title']),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(subforum['description']),
            Row(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                  child: Text(
                    'Теми: ${subforum['topics_num']}',
                    style: TextStyle(fontSize: 12),
                  ),
                ),
                Text(
                  'Мислења: ${subforum['posts_num']}',
                  style: TextStyle(fontSize: 12),
                ),
              ],
            ),
            if (subforum.containsKey('last_post'))
              Text(
                'Последно мислење: ${subforum['last_post']['username']} (${subforum['last_post']['date']})',
                style: TextStyle(fontSize: 12),
              ),
          ],
        ),
        onTap: () {
          Navigator.pushNamed(
            context,
            '/topics',
            arguments: TopicsScreenArguments(
              sid: subforum['id']['\$id'],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      title: 'Forums',
      body: GroupListView(
        sectionsCount: _forums.keys.toList().length,
        countOfItemInSection: (int section) {
          return _forums.values.toList()[section].length;
        },
        itemBuilder: _itemBuilder,
        groupHeaderBuilder: (BuildContext context, int section) {
          return Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: Container(
                  color: Colors.lightBlueAccent,
                  height: 40,
                  padding: const EdgeInsets.fromLTRB(5, 0, 0, 0),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      _forums.keys.toList()[section],
                      style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                      textAlign: TextAlign.left,
                    ),
                  )));
        },
        separatorBuilder: (context, index) => Divider(
          color: Colors.black,
          height: 1,
        ),
        sectionSeparatorBuilder: (context, section) => SizedBox(),
      ),
    );
  }
}
