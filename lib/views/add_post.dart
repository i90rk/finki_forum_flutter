import 'package:finki_forum_flutter/services/route_generator.dart';
import 'package:flutter/material.dart';
import 'package:finki_forum_flutter/views/shared/my_scaffold.dart';

import 'package:flutter/foundation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;
import 'package:finki_forum_flutter/assets/constants.dart' as Constants;

import 'package:provider/provider.dart';
import 'dart:convert';
import 'package:finki_forum_flutter/models/auth.dart';
import 'package:mime/mime.dart';
import 'package:http_parser/http_parser.dart';
import 'package:finki_forum_flutter/views/multimedia_picker.dart';

class AddPost extends StatefulWidget {
  final String sid;
  final String tid;
  final String topicTitle;
  final Map<String, String>? quoteData;

  const AddPost({
    Key? key,
    required this.sid,
    required this.tid,
    required this.topicTitle,
    this.quoteData,
  }) : super(key: key);

  @override
  State<AddPost> createState() => _AddPostState();
}

class _AddPostState extends State<AddPost> {
  XFile? _multimediaData;
  final post = TextEditingController();

  @override
  void initState() {
    super.initState();
    if (widget.quoteData != null) {
      var appendText =
          '[quote][quoteUsername]${widget.quoteData!['username']}[/quoteUsername][quotePost]${widget.quoteData!['post']}[/quotePost][/quote]';
      post.text = appendText;
    }
  }

  void _setMultimedia(XFile media) {
    setState(() => _multimediaData = media);
  }

  Future<void> addPostHandler() async {
    try {
      if (post.text.isEmpty) {
        print('Add Post: Validation Failed');
        return null;
      }

      var userdata = context.read<AuthModel>().userdata;
      var request = http.MultipartRequest(
        'POST',
        Uri.parse('${Constants.SERVER_ADDRESS}/posts/addNewPostMobile'),
      );
      request.fields['subforum_id'] = widget.sid;
      request.fields['topic_id'] = widget.tid;
      request.fields['topic_title'] = widget.topicTitle;
      request.fields['post'] = post.text;

      request.fields['user_id'] = userdata['id']['\$id'];
      request.fields['username'] = userdata['username'];
      request.fields['avatar_image'] = userdata['avatar_image'];
      request.fields['join_date'] = userdata['join_date']['sec'].toString();
      request.fields['group_type'] = userdata['group_type'];

      if (_multimediaData != null) {
        List<String> contentType =
            lookupMimeType(_multimediaData!.name)!.split('/');
        request.files.add(
          await http.MultipartFile.fromPath(
            'file',
            _multimediaData!.path,
            filename: _multimediaData!.name,
            contentType: new MediaType(contentType[0], contentType[1]),
          ),
        );
      }

      var response = await request.send();
      var responsed = await http.Response.fromStream(response);
      final responseData = jsonDecode(responsed.body);
      if (responseData.toString() == '1') {
        Navigator.pushNamed(
          context,
          '/posts',
          arguments: PostsScreenArguments(
            sid: widget.sid,
            tid: widget.tid,
            topicTitle: widget.topicTitle,
          ),
        );
      }
    } catch (err) {
      print('Add Post Error');
      print(err);
    }
  }

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      title: 'Додади мислење',
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              children: <Widget>[
                TextField(
                  minLines: 8,
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  controller: post,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Вашето мислење',
                    hintText: 'Внесете мислење',
                  ),
                ),
                SizedBox(height: 15),
                MultimediaPicker(_multimediaData, _setMultimedia),
                SizedBox(height: 15),
                ElevatedButton.icon(
                  icon: Icon(
                    Icons.post_add,
                    color: Colors.white,
                    size: 24.0,
                  ),
                  style: ElevatedButton.styleFrom(
                    primary: Colors.blue,
                  ),
                  label: Text('Додади мислење'),
                  onPressed: () {
                    addPostHandler();
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
