import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class MultimediaPicker extends StatelessWidget {
  final XFile? multimediaData;
  final Function setMultimedia;
  final ImagePicker _picker = ImagePicker();

  MultimediaPicker(this.multimediaData, this.setMultimedia);

  _imgFromCamera() async {
    final XFile? media = await _picker.pickImage(
      source: ImageSource.camera,
    );
    setMultimedia(media);
  }

  _videoFromCamera() async {
    final XFile? media = await _picker.pickVideo(
      source: ImageSource.camera,
    );
    setMultimedia(media);
  }

  _imgFromGallery() async {
    final XFile? media = await _picker.pickImage(
      source: ImageSource.gallery,
    );
    setMultimedia(media);
  }

  _videoFromGallery() async {
    final XFile? media = await _picker.pickVideo(
      source: ImageSource.gallery,
    );
    setMultimedia(media);
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Фото од галерија'),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Видео од галерија'),
                      onTap: () {
                        _videoFromGallery();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Фото од камера'),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                  new ListTile(
                    leading: new Icon(Icons.videocam),
                    title: new Text('Видео од камера'),
                    onTap: () {
                      _videoFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return ElevatedButton.icon(
      icon: Icon(
        Icons.mms,
        color: Colors.white,
        size: 24.0,
      ),
      style: ElevatedButton.styleFrom(
        primary: multimediaData == null ? Colors.blue : Colors.green,
      ),
      label: Text('Додади мултимедија'),
      onPressed: () {
        _showPicker(context);
      },
    );
  }
}
