import 'package:flutter/material.dart';
import 'package:finki_forum_flutter/views/shared/my_drawer.dart';

class MyScaffold extends StatelessWidget {
  final Widget body;
  final String title;
  final FloatingActionButton? fab;
  final FloatingActionButtonLocation? fabLocation;

  MyScaffold({
    required this.body,
    required this.title,
    this.fab,
    this.fabLocation,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(this.title)),
      body: this.body,
      drawer: MyDrawer(),
      floatingActionButton: this.fab,
      floatingActionButtonLocation: this.fabLocation,
    );
  }
}
