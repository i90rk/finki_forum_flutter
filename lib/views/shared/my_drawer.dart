import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:finki_forum_flutter/models/auth.dart';

class MyDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          if (context.watch<AuthModel>().isLoggedIn)
            DrawerHeader(
              child: ListTile(
                leading: Icon(
                  Icons.account_circle,
                  size: 40,
                ),
                title: Text(
                    '${context.watch<AuthModel>().userdata['firstname']} ${context.watch<AuthModel>().userdata['lastname']}'),
                subtitle: Text(
                    '${context.watch<AuthModel>().userdata['group_type']}'),
              ),
            )
          else
            SizedBox(
              height: 30,
            ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text('Почетна'),
            onTap: () {
              Navigator.pushNamed(
                context,
                '/',
              );
            },
          ),
          context.watch<AuthModel>().isLoggedIn
              ? Column(
                  children: <Widget>[
                    ListTile(
                      leading: Icon(Icons.person),
                      title: Text('Мој профил'),
                      onTap: () {
                        Navigator.pushNamed(
                          context,
                          '/',
                        );
                      },
                    ),
                    ListTile(
                      leading: Icon(Icons.logout),
                      title: Text('Одјави се'),
                      onTap: () {
                        var authContext = context.read<AuthModel>();
                        authContext.setAuthData('', {}, false);
                        Navigator.pushNamed(
                          context,
                          '/',
                        );
                      },
                    ),
                  ],
                )
              : Column(
                  children: <Widget>[
                    ListTile(
                      leading: Icon(Icons.login),
                      title: Text('Логирај се'),
                      onTap: () {
                        Navigator.pushNamed(
                          context,
                          '/login',
                        );
                      },
                    ),
                    ListTile(
                      leading: Icon(Icons.how_to_reg),
                      title: Text('Регистрирај се'),
                      onTap: () {
                        Navigator.pushNamed(
                          context,
                          '/register',
                        );
                      },
                    ),
                  ],
                )
        ],
      ),
    );
  }
}
