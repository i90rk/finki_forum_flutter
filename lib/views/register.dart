import 'package:flutter/material.dart';
import 'package:finki_forum_flutter/views/shared/my_scaffold.dart';

class Register extends StatefulWidget {
  const Register({
    Key? key,
  }) : super(key: key);

  @override
  State<Register> createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final firstname = TextEditingController();
  final lastname = TextEditingController();
  final username = TextEditingController();
  final password = TextEditingController();
  final confirmPassword = TextEditingController();
  final email = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      title: 'Регистрирај се',
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              children: <Widget>[
                TextField(
                  controller: firstname,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Име',
                    hintText: 'Внеси име',
                  ),
                ),
                SizedBox(height: 10),
                TextField(
                  controller: lastname,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Презиме',
                    hintText: 'Внеси презиме',
                  ),
                ),
                SizedBox(height: 10),
                TextField(
                  controller: username,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Корисничко име',
                    hintText: 'Внеси корисничко име',
                  ),
                ),
                SizedBox(height: 10),
                TextField(
                  controller: password,
                  obscureText: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Лозинка',
                    hintText: 'Внеси безбедна лозинка',
                  ),
                ),
                SizedBox(height: 10),
                TextField(
                  controller: confirmPassword,
                  obscureText: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Потврди лозинка',
                    hintText: 'Потврди лозинка',
                  ),
                ),
                SizedBox(height: 10),
                TextField(
                  controller: email,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Емаил адреса',
                    hintText: 'Внеси емаил адрес',
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  height: 50,
                  width: 250,
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: TextButton(
                    child: Text(
                      'Регистрирај се',
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                    onPressed: () {},
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
