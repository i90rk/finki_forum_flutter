import 'package:finki_forum_flutter/views/add_topic.dart';
import 'package:finki_forum_flutter/views/register.dart';
import 'package:flutter/material.dart';

import 'package:finki_forum_flutter/views/home.dart';
import 'package:finki_forum_flutter/views/login.dart';
import 'package:finki_forum_flutter/views/topics.dart';
import 'package:finki_forum_flutter/views/posts.dart';
import 'package:finki_forum_flutter/views/add_post.dart';

class TopicsScreenArguments {
  final String sid; // subforum id

  const TopicsScreenArguments({
    required this.sid,
  });
}

class PostsScreenArguments {
  final String sid; // subforum id
  final String tid; // topic id
  final String topicTitle;
  final Map<String, String>? quoteData;

  const PostsScreenArguments({
    required this.sid,
    required this.tid,
    required this.topicTitle,
    this.quoteData,
  });
}

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (context) => Home());
      case '/login':
        return MaterialPageRoute(builder: (context) => Login());
      case '/register':
        return MaterialPageRoute(builder: (context) => Register());
      case '/topics':
        final args = settings.arguments as TopicsScreenArguments;
        return MaterialPageRoute(
          builder: (context) => Topics(
            sid: args.sid,
          ),
        );
      case '/posts':
        final args = settings.arguments as PostsScreenArguments;
        return MaterialPageRoute(
          builder: (context) => Posts(
            sid: args.sid,
            tid: args.tid,
            topicTitle: args.topicTitle,
          ),
        );
      case '/add-topic':
        final args = settings.arguments as TopicsScreenArguments;
        return MaterialPageRoute(
          builder: (context) => AddTopic(
            sid: args.sid,
          ),
        );
      case '/add-post':
        final args = settings.arguments as PostsScreenArguments;
        return MaterialPageRoute(
          builder: (context) => AddPost(
            sid: args.sid,
            tid: args.tid,
            topicTitle: args.topicTitle,
            quoteData: args.quoteData,
          ),
        );
      default:
        // If there is no such named route in the switch statement
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (context) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
        body: Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}
