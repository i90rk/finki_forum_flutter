import 'package:flutter/foundation.dart';

class AuthModel extends ChangeNotifier {
  bool _isLoggedIn = false;
  String _token = '';
  Map<String, dynamic> _userdata = {};

  void setAuthData(
      String token, Map<String, dynamic> userdata, bool isLoggedIn) {
    _isLoggedIn = isLoggedIn;
    _token = token;
    _userdata = userdata;
    notifyListeners();
  }

  bool get isLoggedIn => _isLoggedIn;
  String get token => _token;
  Map<String, dynamic> get userdata => _userdata;
}
